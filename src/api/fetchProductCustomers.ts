import { apiUrl } from '../config'
import { IProductWithCustomers } from '../models'

export const fetchProductCustomers = async (id: string): Promise<IProductWithCustomers> => {
    const res = await fetch(`${apiUrl}/products/${id}`)

    return res.json()
}
