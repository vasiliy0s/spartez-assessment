import { apiUrl } from '../config'
import { IProduct } from '../models'

export const fetchProducts = async (): Promise<IProduct[]> => {
    const res = await fetch(`${apiUrl}/products`)

    return res.json()
}
