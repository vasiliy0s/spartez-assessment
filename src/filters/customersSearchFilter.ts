import { ICustomer } from '../models'

export const customersSearchFilter = (customers: ICustomer[], searchStr: string): ICustomer[] => {
    const nSearchString = normaliseString(searchStr)

    if (!nSearchString.length) return customers

    return customers.filter((customer) => {
        const nQuote = normaliseString(customer.quote)
        const nName = normaliseString(customer.name)

        return nQuote.includes(nSearchString) || nName.includes(nSearchString)
    })
}

const normaliseString = (str: string): string =>
    String(str || '')
        .toLocaleLowerCase()
        .replace(/[^A-Za-z0-9]+/g, '')
