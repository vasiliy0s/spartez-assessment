import React, { FC } from 'react'
import './product.css'
import { IProduct } from '../../../models'
import { Customers } from '../../customers'
import { useProductCustomers } from '../../../hooks'
import { productCustomersPollIntervalMilliseconds } from '../../../config'
import { useFilteredProductCustomers } from '../../../hooks/useFilteredCustomers'

interface IProps {
    product: IProduct
    search: string
}

export const Product: FC<IProps> = ({ product, search }) => {
    const { initialLoading, product: productCustomers } = useProductCustomers(
        product.id,
        productCustomersPollIntervalMilliseconds
    )

    const customers = useFilteredProductCustomers(productCustomers, search)

    return (
        <article className="product">
            <h2 className="product--name">{product.name}</h2>
            <Customers loading={initialLoading} customers={customers} search={search} />
        </article>
    )
}
