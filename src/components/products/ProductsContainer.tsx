import React, { FC } from 'react'
import { Product } from './product'
import { IProduct } from '../../models'
import './products.css'

interface IProps {
    products: IProduct[]
    search: string
}

export const ProductsContainer: FC<IProps> = ({ products, search }) => (
    <section className="products">
        {products.map((product) => (
            <Product product={product} search={search} key={product.id} />
        ))}
    </section>
)
