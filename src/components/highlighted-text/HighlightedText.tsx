import React, { FC } from 'react'
import Highlighter from 'react-highlight-words'
import isString from 'lodash/isString'

interface IProps {
    text: string
    search: string
}

export const HighlightedText: FC<IProps> = ({ text, search }) =>
    isString(search) && search.length ? (
        <Highlighter
            searchWords={[search]}
            textToHighlight={text}
            autoEscape
            highlightClassName="highlighted-text"
        />
    ) : (
        <span>{text}</span>
    )
