import React, { FC, useState, useEffect } from 'react'
import './search.css'

interface IProps {
    onChange(search: string): void
}

export const SearchBar: FC<IProps> = ({ onChange }) => {
    const [search, setSearch] = useState<string>('')

    useEffect(() => {
        onChange(search)
    }, [search, onChange])

    return (
        <div className="search-bar">
            <input
                type="search"
                data-testid="search-input"
                value={search}
                onChange={(evt) => setSearch(evt.target.value)}
                className="search-bar--search"
                placeholder="Start typing to highlight..."
            />
        </div>
    )
}
