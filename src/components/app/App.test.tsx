import React from 'react'
import { render } from '@testing-library/react'
import { App } from './App'

describe('App', () => {
    it('renders the app with search-input', () => {
        const { getByTestId } = render(<App />)
        const searchElement = getByTestId('search-input')
        expect(searchElement).toBeInstanceOf(HTMLInputElement)
    })
})
