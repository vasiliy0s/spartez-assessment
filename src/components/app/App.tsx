import React, { useState, useMemo, useEffect } from 'react'
import debounce from 'lodash/debounce'
import { searchDebounceDelay } from '../../config'
import { SearchBar } from '../search'
import { ProductsContainer } from '../products'
import { useProducts } from '../../hooks'

export const App: React.FC = () => {
    const products = useProducts()
    const [search, setSearch] = useState<string>('')

    const debouncedOnChange = useMemo(() => debounce(setSearch, searchDebounceDelay), [setSearch])
    useEffect(() => () => debouncedOnChange.cancel(), [debouncedOnChange])

    return (
        <div>
            <SearchBar onChange={debouncedOnChange} />
            <ProductsContainer products={products} search={search} />
        </div>
    )
}
