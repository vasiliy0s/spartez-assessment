import React, { FC } from 'react'
import './loading-customers.css'

export const LoadingCustomers: FC = () => (
    <li>
        <h5 className="loading-customers--title">Loading...</h5>
    </li>
)
