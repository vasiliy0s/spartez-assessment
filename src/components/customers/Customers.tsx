import React, { FC } from 'react'
import './customers.css'
import { ICustomer } from '../../models'
import { Customer } from './customer'
import { NoCustomers } from './no-customers'
import { LoadingCustomers } from './loading-customers'

interface IProps {
    customers: ICustomer[]
    search: string
    loading?: boolean
}

export const Customers: FC<IProps> = ({ loading, customers, search }) => (
    <ul className="customers">
        {loading && <LoadingCustomers />}

        {!loading &&
            (customers.length ? (
                customers.map((customer) => (
                    <Customer key={customer.id} customer={customer} search={search} />
                ))
            ) : (
                <NoCustomers />
            ))}
    </ul>
)
