import React, { FC } from 'react'
import './no-customers.css'

export const NoCustomers: FC = () => (
    <li>
        <h4 className="no-customers--title">No Customers</h4>
    </li>
)
