import React, { FC } from 'react'
import { ICustomer } from '../../../models'
import { HighlightedText } from '../../highlighted-text'
import './customer.css'

interface IProps {
    customer: ICustomer
    search: string
}

export const Customer: FC<IProps> = ({ customer, search }) => (
    <li className="customer">
        <div className="customer--info">
            <img className="customer--avatar" src={customer.avatar} alt={customer.name} />
            <h3 className="customer--name">
                <HighlightedText text={customer.name} search={search} />
            </h3>
            <small className="customer--position">{customer.job.title}</small>
            <small className="customer--company">{customer.job.company}</small>
        </div>

        <blockquote className="customer--quote">
            <HighlightedText text={customer.quote} search={search} />
        </blockquote>
    </li>
)
