import ms from 'ms'

export const apiUrl = process.env.REACT_APP_API_HOST_URL
export const productCustomersPollIntervalMilliseconds = ms(
    process.env.REACT_APP_PRODUCT_CUSTOMERS_POLL_INTERVAL || '30 sec'
)
export const searchDebounceDelay = ms(process.env.REACT_APP_SEARCH_DEBOUNCE_DELAY || '250 ms')
