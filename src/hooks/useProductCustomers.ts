import { useState, useEffect } from 'react'
import { IProductWithCustomers } from '../models'
import { fetchProductCustomers } from '../api/fetchProductCustomers'

interface IOutput {
    initialLoading: boolean
    product: IProductWithCustomers | null
}

export const useProductCustomers = (id: string, pollIntervalMilliseconds: number): IOutput => {
    const [initialLoading, setInitialLoading] = useState<boolean>(true)
    const [productWithCustomers, setProductWithCustomers] = useState<IProductWithCustomers | null>(
        null
    )

    useEffect(() => {
        let killed = false
        let tid: number

        const fetchData = async () => {
            const data = await fetchProductCustomers(id)

            if (!killed) {
                setProductWithCustomers(data)
                setInitialLoading(false)
            }
        }

        tid = window.setInterval(fetchData, pollIntervalMilliseconds)

        fetchData()

        return () => {
            killed = true
            clearInterval(tid)
        }
    }, [id, pollIntervalMilliseconds])

    return {
        product: productWithCustomers,
        initialLoading,
    }
}
