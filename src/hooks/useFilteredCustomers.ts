import { useMemo } from 'react'
import { ICustomer, IProductWithCustomers } from '../models'
import { customersSearchFilter } from '../filters'

export const useFilteredProductCustomers = (
    productCustomers: IProductWithCustomers | null,
    search: string
): ICustomer[] =>
    useMemo<ICustomer[]>(() => {
        if (productCustomers == null) return []

        return customersSearchFilter(productCustomers.customers, search)
    }, [search, productCustomers])
