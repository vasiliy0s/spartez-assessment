import { useState, useEffect } from 'react'
import { IProduct } from '../models'
import { fetchProducts } from '../api'

export const useProducts = (cacheKey = 'cache'): IProduct[] => {
    const [products, setProducts] = useState<IProduct[]>([])

    useEffect(() => {
        let killed = false

        const fetchData = async () => {
            const data = await fetchProducts()

            if (!killed) {
                setProducts(data)
            }
        }

        fetchData()

        return () => {
            killed = true
        }
    }, [cacheKey])

    return products
}
