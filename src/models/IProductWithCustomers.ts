export interface ICustomerAddress {
    street: string
    city: string
    zipCode: string // number in string
    country: string
}

export interface ICustomerJob {
    title: string
    company: string
}

export interface ICustomer {
    id: number
    name: string
    dateOfBirth: string // '1972-07-22T18:36:17.786Z'
    address: ICustomerAddress
    phone: string // '004-214-0807'
    username: string // 'Issac_Windler'
    email: string
    avatar: string // image url
    job: ICustomerJob
    quote: string
}

export interface IProductWithCustomers {
    id: string
    name: string
    customers: ICustomer[]
}
